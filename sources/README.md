## 译文
Stride 源码
=============

文件夹和项目布局
---------------------------

### 核心 ###

* __Stride.Core__:
   引用计数，依赖项属性系统(属性容器/属性键值)，低级序列化，低级内存操作（实用程序和本地流）
* __Stride.Core.Mathematics__:
   数学库（虽说是这个名称，不依赖于Stride.Core）
* __Stride.Core.IO__:
   虚拟文件系统
* __Stride.Core.Serialization__:
   高级序列化和类似git的CAS(固定内容寻址存储)存储系统
* __Stride.Core.MicroThreading__:
   基于C＃5.0异步的微线程库(又名无堆栈编程)
* __Stride.Core.AssemblyProcessor__:
   用于修补程序集以添加各种功能的内部工具，例如序列化自动生成，各种内存/固定操作，模块初始化程序等。。。
   
### 图像 ###

* __Stride.Core.Presentation__: WPF UI库(主题，控件，例如属性网格，行为等)
* __Stride.Core.SampleApp__: 简单的属性网格示例
* __Stride.Core.Quantum__: 先进的视图模型库，可以通过网络(带差异)以请求的时间间隔同步视图模型。这样，可以在引擎内定义视图模型而无需任何UI依赖项。

### 构建引擎 ###

* __Stride.Core.BuildEngine.Common__:
   构建引擎的通用部分。可以重复使用它来添加新的构建步骤，构建命令以及构建新的自定义构建引擎客户端
* __Stride.Core.BuildEngine__: 生成引擎工具的默认实现(可执行)
* __Stride.Core.BuildEngine.Monitor__: WPF显示构建引擎的实时结果(类似于IncrediBuild)
* __Stride.Core.BuildEngine.Editor__: WPF生成引擎规则编辑器并被大多数项目使用。

### 着色器 ###

* __Irony__: 解析库，由Stride.Core.Shaders使用。以后应由ANTLR4代替。
* __Stride.Core.Shaders__: 着色器解析，类型分析和转换库(由HLSL到GLSL和Stride着色器语言使用)

### 目标 ###

* MSBuild目标文件可轻松创建跨平台解决方案（Android，iOS，WinRT，WinPhone等），并全局定义行为和目标。可扩展。

----------

在您的项目中使用
-------------------

### 源库 ###

有两种方法可以将此存储库集成到您自己的存储库中:

* __git subtree__ [documentation](https://github.com/git/git/blob/master/contrib/subtree/git-subtree.txt) and [blog post](http://psionides.eu/2010/02/04/sharing-code-between-projects-with-git-subtree/)
* __git submodule__

### 基本用途 ###

只需在Visual Studio解决方案中直接添加要使用的项目即可

### 可选：添加程序集处理器 ###

如果要使用自动生成的序列化`Serialization`代码，某些实用工具`Utilities`功能或模块初始化器`ModuleInitializer`，则需要使用 __Stride.Core.AssemblyProcessor__

步骤:

* 在解决方案中同时包含 __Stride.Core.AssemblyProcessor__ 和 __Stride.Core.AssemblyProcessor.Common__ 
* 在您的解决方案文件夹中添加 __Stride.Core.PostSettings.Local.targets__ 或 __YourSolutionName.PostSettings.Local.targets__ ，内容如下：

```xml
<!-- 解决方案中的所有项目自动自动包含的构建文件 -->
<Project xmlns="http://schemas.microsoft.com/developer/msbuild/2003">
  <PropertyGroup>
    <!-- Enable assembly processor -->
    <StrideAssemblyProcessorGlobal>true</StrideAssemblyProcessorGlobal>
  </PropertyGroup>
</Project>
```
## 原文
Stride Sources
=============

Folders and projects layout
---------------------------

### core ###

* __Stride.Core__:
   Reference counting, dependency property system (PropertyContainer/PropertyKey), low-level serialization, low-level memory operations (Utilities and NativeStream).
* __Stride.Core.Mathematics__:
   Mathematics library (despite its name, no dependencies on Stride.Core).
* __Stride.Core.IO__:
   Virtual File System.
* __Stride.Core.Serialization__:
   High-level serialization and git-like CAS storage system.
* __Stride.Core.MicroThreading__:
   Micro-threading library based on C# 5.0 async (a.k.a. stackless programming)
* __Stride.Core.AssemblyProcessor__:
   Internal tool used to patch assemblies to add various features, such as Serialization auto-generation, various memory/pinning operations, module initializers, etc...
   
### presentation ###

* __Stride.Core.Presentation__: WPF UI library (themes, controls such as propertygrid, behaviors, etc...)
* __Stride.Core.SampleApp__: Simple property grid example.
* __Stride.Core.Quantum__: Advanced ViewModel library that gives ability to synchronize view-models over network (w/ diff), and at requested time intervals. That way, view models can be defined within engine without any UI dependencies.

### buildengine ###

* __Stride.Core.BuildEngine.Common__:
   Common parts of the build engine. It can be reused to add new build steps, build commands, and also to build a new custom build engine client.
* __Stride.Core.BuildEngine__: Default implementation of build engine tool (executable)
* __Stride.Core.BuildEngine.Monitor__: WPF Display live results of build engine (similar to IncrediBuild)
* __Stride.Core.BuildEngine.Editor__: WPF Build engine rules editor
and used by most projects.

### shader ###

* __Irony__: Parsing library, used by Stride.Core.Shaders. Should later be replaced by ANTLR4.
* __Stride.Core.Shaders__: Shader parsing, type analysis and conversion library (used by HLSL->GLSL and Stride Shader Language)

### targets ###

* MSBuild target files to create easily cross-platform solutions (Android, iOS, WinRT, WinPhone, etc...), and define behaviors and targets globally. Extensible.

----------

Use in your project
-------------------

### Source repository ###

There is two options to integrate this repository in your own repository:

* __git subtree__ [documentation](https://github.com/git/git/blob/master/contrib/subtree/git-subtree.txt) and [blog post](http://psionides.eu/2010/02/04/sharing-code-between-projects-with-git-subtree/)
* __git submodule__

### Basic use ###

Simply add the projects you want to use directly in your Visual Studio solution.

### Optional: Activate assembly processor ###

If you want to use auto-generated `Serialization` code, some of `Utilities` functions or `ModuleInitializer`, you need to use __Stride.Core.AssemblyProcessor__.

Steps:

* Include both __Stride.Core.AssemblyProcessor__ and __Stride.Core.AssemblyProcessor.Common__ in your solution.
* Add either a __Stride.Core.PostSettings.Local.targets__ or a __YourSolutionName.PostSettings.Local.targets__ in your solution folder, with this content:

```xml
<!-- Build file pre-included automatically by all projects in the solution -->
<Project xmlns="http://schemas.microsoft.com/developer/msbuild/2003">
  <PropertyGroup>
    <!-- Enable assembly processor -->
    <StrideAssemblyProcessorGlobal>true</StrideAssemblyProcessorGlobal>
  </PropertyGroup>
</Project>
```
