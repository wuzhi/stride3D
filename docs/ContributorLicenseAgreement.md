# Stride个人贡献者许可协议

感谢您有兴趣为Stride做出贡献。

本贡献者协议记录了贡献者授予我们的权利。为使本文档有效，请按照[CLA助手](https://github.com/CLAassistant)给出的说明签名并通过电子提交将其发送给我们，该助手将在您的第一个请求请求中发送评论。这是具有法律约束力的文档，因此在同意之前请仔细阅读。该协议可能涵盖我们管理的多个软件项目。

## 1. 定义

"您"是指向我们做出贡献的个人。

“贡献”是指您拥有或主张版权的任何由您提交给我们的著作权。如果您在整个著作权中不拥有版权，请按照https://github.com/stride3d/stride中的说明进行操作。

“版权”是指由您拥有或控制的著作权的所有版权保护，包括版权，精神和邻接权（视情况而定），在其存在的整个期限内，包括您的任何扩展。

“材料”是指我们提供给第三方的著作权作品。当本协议涵盖多个软件项目时，“材料”指的是提交稿件的作者作品。提交文稿后，它可能会包含在材料中。

“提交”是指发送给我们或我们的代表的任何形式的电子，口头或书面通讯，包括但不限于由我们管理或代表我们管理的电子邮件列表，源代码控制系统和问题跟踪系统为了讨论和改进本资料，但不包括您明显标记或以其他方式书面指定为“非贡献”的交流。

“提交日期”是指您向我们提交捐助的日期。

“生效日期”指您执行本协议的日期或您首次向我们提交捐款的日期，以较早者为准。

## 2. 权利授予

### 2.1 版权许可

(a) 您保留自己的贡献中的版权所有权，并且具有使用或许可该贡献的相同权利，而无需签署协议即可拥有。

(b) 在相关法律允许的最大范围内，您授予我们永久性的，全球性的，非排他性的，可转让的，免版税的，不可撤消的永久版权许可，涉及该贡献，并通过多层分许可将这些权利分许可，以复制，修改，显示，执行和分发作为材料一部分的贡献；前提是该许可须遵守第2.3节的规定。

### 2.2 专利许可

对于专利权利要求，包括但不限于您现在，将来拥有，控制或有权授予的方法，过程和设备权利，您向我们授予永久，全球性，非排他性，可转让的特许权使用费，免费的，不可撤销的专利许可，并具有将这些权利分许可给多层分许可，制造，制造，使用，出售，要约出售，进口以及以其他方式与材料（及部分）结合转让和出资的权利这样的组合）。仅在行使许可权利侵犯此类专利要求的范围内授予本许可；并且该许可的前提是遵守第2.3节。

### 2.3 出境许可证

基于第2.1节和第2.2节中的权利授予，如果我们将您的贡献包括在材料中，则我们可以根据任何许可（包括copyleft，宽松，商业或专有许可）许可该贡献。

### 2.4 精神权利

如果精神权利在法律允许的最大范围内适用于本贡献，则您放弃并同意不对我们或我们的继任人或任何我们的被许可人直接或间接地主张这种精神权利。

### 2.5 我们的权利

您承认我们没有义务将您的贡献用作材料的一部分，并可能决定包括我们认为适当的任何贡献。

### 2.6 保留权利

您明确保留未根据本节明确许可的任何权利。

## 3. 协议

您确认:

(a) 您具有签署本协议的合法权限。

(b) 您拥有涉及该贡献的版权和专利要求，而这些权利要求要授予第2节下的权利。

(c) 根据第2节授予的权利不违反您对第三方（包括您的雇主）所作的任何权利授予。如果您是雇员，则表明您的雇主批准了本协议或签署了本文档的实体版本。如果您未满18岁，请让您的父母或监护人签署协议。

(d) 如果您在提交的全部著作权中都不拥有版权，请遵循https://github.com/stride3d/stride中的说明。

## 4. 免责声明

除第3节中的明示保证外，捐款均按“原样”提供。尤其是，您明确拒绝所有明示或暗示的担保，包括但不限于对适销性，特定用途的适用性和非侵权性的任何暗示担保。在无法免除任何此类保证的范围内，此类保证仅限于法律允许的最短期限内。

## 5. 相应的损害豁免

在适用法律允许的最大范围内，对于因本协议而引起的任何利润损失，预期损失，数据损失，间接，特殊，偶然，继发性和示范性损害，在任何情况下，您均不承担任何责任或基于合理的理论（合同，侵权或其他）。

## 6. 其他

6.1 本协议受排除其法律冲突规定的法律约束，并受其解释。在某些情况下，本节中的管辖法律可能会被《联合国国际货物销售合同公约》（"《联合国公约》"）取代，并且当事各方打算避免将《联合国公约》适用于本协议，因此，排除了《联合国公约》在本协议中的全部适用范围。

6.2 本协议列出了您与我们之间关于您对我们的贡献的全部协议，并覆盖了所有其他协议或谅解。

6.3 如果您或我们将通过本协议获得的权利或义务转让给第三方，作为转让的条件，则该第三方必须以书面形式同意遵守协议中的所有权利和义务。

6.4 一方未能在另一种情况下要求另一方履行本协议的任何规定，并不影响一方在将来任何时候要求执行该协议的权利。在一种情况下放弃某项规定的履约，不应视为对该规定的未来放弃或该规定的全部放弃。

6.5 如果发现本协议的任何条款无效且无法执行，则将在可能的范围内以最接近原始条款含义且可以执行的条款替换该条款。即使本协议的基本目的未能履行或在法律允许的最大范围内进行任何有限的补救，本协议中规定的条款和条件仍然适用。


# Stride Individual Contributor License Agreement

Thank you for your interest in contributing to Stride ("We" or "Us").

This contributor agreement ("Agreement") documents the rights granted by contributors to Us. To make this document effective, please sign it and send it to Us by electronic submission, following the instructions given by [CLA assistant](https://github.com/CLAassistant) who will send a comment in your first pull request. This is a legally binding document, so please read it carefully before agreeing to it. The Agreement may cover more than one software project managed by Us.

## 1. Definitions

"You" means the individual who Submits a Contribution to Us.

"Contribution" means any work of authorship that is Submitted by You to Us in which You own or assert ownership of the Copyright. If You do not own the Copyright in the entire work of authorship, please follow the instructions in https://github.com/stride3d/stride.

"Copyright" means all rights protecting works of authorship owned or controlled by You, including copyright, moral and neighboring rights, as appropriate, for the full term of their existence including any extensions by You.

"Material" means the work of authorship which is made available by Us to third parties. When this Agreement covers more than one software project, the Material means the work of authorship to which the Contribution was Submitted. After You Submit the Contribution, it may be included in the Material.

"Submit" means any form of electronic, verbal, or written communication sent to Us or our representatives, including but not limited to electronic mailing lists, source code control systems, and issue tracking systems that are managed by, or on behalf of, Us for the purpose of discussing and improving the Material, but excluding communication that is conspicuously marked or otherwise designated in writing by You as "Not a Contribution."

"Submission Date" means the date on which You Submit a Contribution to Us.

"Effective Date" means the date You execute this Agreement or the date You first Submit a Contribution to Us, whichever is earlier.

## 2. Grant of Rights

### 2.1 Copyright License

(a) You retain ownership of the Copyright in Your Contribution and have the same rights to use or license the Contribution which You would have had without entering into the Agreement.

(b) To the maximum extent permitted by the relevant law, You grant to Us a perpetual, worldwide, non-exclusive, transferable, royalty-free, irrevocable license under the Copyright covering the Contribution, with the right to sublicense such rights through multiple tiers of sublicensees, to reproduce, modify, display, perform and distribute the Contribution as part of the Material; provided that this license is conditioned upon compliance with Section 2.3.

### 2.2 Patent License

For patent claims including, without limitation, method, process, and apparatus claims which You  own, control or have the right to grant, now or in the future, You grant to Us a perpetual, worldwide, non-exclusive, transferable, royalty-free, irrevocable patent license, with the right to sublicense these rights to multiple tiers of sublicensees, to make, have made, use, sell, offer for sale, import and otherwise transfer the Contribution and the Contribution in combination with the Material (and portions of such combination). This license is granted only to the extent that the exercise of the licensed rights infringes such patent claims; and provided that this license is conditioned upon compliance with Section 2.3.

### 2.3 Outbound License

Based on the grant of rights in Sections 2.1 and 2.2, if We include Your Contribution in a Material, We may license the Contribution under any license, including copyleft, permissive, commercial, or proprietary licenses.

### 2.4 Moral Rights

If moral rights apply to the Contribution, to the maximum extent permitted by law, You waive and agree not to assert such moral rights against Us or our successors in interest, or any of our licensees, either direct or indirect.

### 2.5 Our Rights

You acknowledge that We are not obligated to use Your Contribution as part of the Material and may decide to include any Contribution We consider appropriate.

### 2.6 Reservation of Rights

Any rights not expressly  licensed under this section are expressly reserved by You.

## 3. Agreement

You confirm that:

(a) You have the legal authority to enter into this Agreement.

(b) You  own the Copyright and patent claims covering the Contribution which are required to grant the rights under Section 2.

(c) The grant of rights under Section 2 does not violate any grant of rights which You have made to third parties, including Your employer.  If You are an employee, You have had Your employer approve this Agreement or sign the Entity version of this document.  If You are less than eighteen years old, please have Your parents or guardian sign the Agreement.

(d) You have followed the instructions in https://github.com/stride3d/stride, if You do not own the Copyright in the entire work of authorship Submitted.

## 4. Disclaimer

EXCEPT FOR THE EXPRESS WARRANTIES IN SECTION 3, THE CONTRIBUTION IS PROVIDED "AS IS". MORE PARTICULARLY, ALL EXPRESS OR IMPLIED WARRANTIES INCLUDING, WITHOUT LIMITATION, ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE EXPRESSLY DISCLAIMED BY YOU TO US. TO THE EXTENT THAT ANY SUCH WARRANTIES CANNOT BE DISCLAIMED, SUCH WARRANTY IS LIMITED IN DURATION TO THE MINIMUM PERIOD PERMITTED BY LAW.

## 5. Consequential Damage Waiver

TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT WILL YOU  BE LIABLE FOR ANY LOSS OF PROFITS, LOSS OF ANTICIPATED SAVINGS, LOSS OF DATA, INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL AND EXEMPLARY DAMAGES ARISING OUT OF THIS AGREEMENT REGARDLESS OF THE LEGAL OR EQUITABLE THEORY (CONTRACT, TORT OR OTHERWISE) UPON WHICH THE CLAIM IS BASED.

## 6. Miscellaneous

6.1 This Agreement will be governed by and construed in accordance with the laws of  excluding its conflicts of law provisions. Under certain circumstances, the governing law in this section might be superseded by the United Nations Convention on Contracts for the International Sale of Goods ("UN Convention") and the parties intend to avoid the application of the UN Convention to this Agreement and, thus, exclude the application of the UN Convention in its entirety to this Agreement.

6.2 This Agreement sets out the entire agreement between You and Us for Your Contributions to Us and overrides all other agreements or understandings.

6.3  If You or We assign the rights or obligations received through this Agreement to a third party, as a condition of the assignment, that third party must agree in writing to abide by all the rights and obligations in the Agreement.

6.4 The failure of either party to require performance by the other party of any provision of this Agreement in one situation shall not affect the right of a party to require such performance at any time in the future. A waiver of performance under a provision in one situation shall not be considered a waiver of the performance of the provision in the future or a waiver of the provision in its entirety.

6.5 If any provision of this Agreement is found void and unenforceable, such provision will be replaced to the extent possible with a provision that comes closest to the meaning of the original provision and which is enforceable.  The terms and conditions set forth in this Agreement shall apply notwithstanding any failure of essential purpose of this Agreement or any limited remedy to the maximum extent possible under law.
