# 本地化

## 翻译

请通过更新现有翻译和/或添加新语言来帮助我们翻译，网址为https://hosted.weblate.org/projects/stride/

翻译从`weblate`分支手动合并回`master`分支。

## 在Game Studio中激活新语言

在Weblate上添加了一种新语言后，需要在构建和启动过程中在Game Studio中将其激活。

请查看commit https://github.com/stride3d/stride/commit/c70f07f449
以获取有关如何在Game Studio中添加新语言的示例。

# Localization

## Translation

Please help us translate by updating existing translations and/or adding new language at https://hosted.weblate.org/projects/stride/

Translation are manually merged back from `weblate` branch to `master` branch.

## Activate new language in Game Studio

Once a new language has been added on weblate, it needs to be activated in the Game Studio during build & startup.

Please check commit https://github.com/stride3d/stride/commit/c70f07f449 for an example on how to add a new language in Game Studio.
