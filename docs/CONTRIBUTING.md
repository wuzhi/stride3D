# 向Stride贡献

## 检查我们的问题跟踪器

请查看我们的[问题追踪器](https://github.com/stride3d/stride/issues)，尤其是标有[help wanted](https://github.com/stride3d/stride/labels/help%20wanted)。

如果您刚刚开始使用Stride，则标有['good first issue'](https://github.com/stride3d/stride/labels/good%20first%20issue)的问题可能是一个很好的切入点。

## 通知用户

一旦开始工作，请在适当的问题上留言，或者如果不存在，请创建一个留言：
* 确保没有其他人正在处理同一问题
* 列出您的计划，并与合作者和用户讨论，以确保其架构正确且适合项目

## 编码风格

更改文件时，请使用并遵循Stride的.editorconfig。

## 提交更改

* 将更改推送到fork中的特定分支。
* 使用该分支创建并向正式存储库填写拉取请求。
* 在创建请求请求后，如果这是您第一次参与[CLA助手](https://cla-assistant.io/)，将要求您签署[贡献者许可协议](https://github.com/stride3d/stride/blob/master/docs/ContributorLicenseAgreement.md)。

# Contributing to Stride

## Check our issue tracker

Please take a look at our [issue tracker](https://github.com/stride3d/stride/issues), especially issues marked with [help wanted](https://github.com/stride3d/stride/labels/help%20wanted).

If you are just getting started with Stride, issues marked with ['good first issue'](https://github.com/stride3d/stride/labels/good%20first%20issue) can be a good entry point.

## Notify users

Once you start working leave a message on the appropriate issue or create one if none exists to:
* make sure that no one else is working on that same issue
* lay out your plans and discuss it with collaborators and users to make sure it is properly architectured and would fit well in the project

## Coding style

Please use and follow Stride's `.editorconfig` when making changes to files.

## Submitting Changes

* Push your changes to a specific branch in your fork.
* Use that branch to create and fill out a pull request to the official repository.
* After creating that pull request and if it's your first time contributing a [CLA assistant](https://cla-assistant.io/) will ask you to sign the [Contributor License Agreement](https://github.com/stride3d/stride/blob/master/docs/ContributorLicenseAgreement.md).
