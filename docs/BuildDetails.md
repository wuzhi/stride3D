﻿# 构建

## 概述

这是对技术的描述，它会在我们的构建中发生什么以及如何组织。这主要涵盖了Stride本身的构建体系结构。

* [目标](../Targets)包含游戏使用的MSBuild目标文件
* [sources/common/targets](../sources/common/targets)(通用)和[sources/targets](../sources/targets)(Stride-特定)包含用于构建Stride本身的MSBuild目标文件。

从3.1开始，我们从自定义构建系统切换到新的csproj系统，每个程序集具有一个nuget包。

我们使用`TargetFrameworks`来通过单个项目（Android，iOS等）正确编译不同的平台。

同样，我们使用`RuntimeIdentifiers`选择图形平台。[MSBuild.Sdk.Extras](https://github.com/onovotny/MSBuildSdkExtras)用于正确地构建带有多个`RuntimeIdentifiers`的NuGet软件包（不支持开箱即用）。

### 局限性

* 依赖关系是每个`RuntimeIdentifier`的，而依赖关系不是每个`RuntimeIdentifier`(跟踪[NuGet#1660](https://github.com/NuGet/Home/issues/1660)).
* FastUpToDate检查不适用于多个`TargetFrameworks`(跟踪[project-system#2487](https://github.com/dotnet/project-system/issues/2487)).

## NuGet 解决

由于我们想使用仅包含与其他NuGet运行时程序包具有适当依赖性的可执行文件的程序包来打包工具（即GameStudio，ConnectionRouter，CompilerApp），因此我们使用NuGet API在运行时解析程序集。

负责此操作的代码位于[Stride.NuGetResolver](../sources/shared/Stride.NuGetResolver).

稍后，我们可能想利用.NET Core依赖项解析来本地完成此操作。另外，我们可能希望使用实际的项目信息/依赖关系来解析为不同的运行时程序集和更好的支持插件。

## 版本控制

我们有3个地方提供版本:
- Stride 包版本 (.sdpkg)
- 程序集版本 (SharedAssemblyInfo.cs) -- 应该与Stride软件包版本保持同步（注意：如果版本不匹配，软件包构建将报告错误）
- NuGet 包版本 (.nupkg) -- 自动衍生自Stride包版本

在软件包构建期间，如果未将`StrideOfficialBuild`设置为true，则将使用`-beta<commits_count_since_last_version_change>-g<git_hash>`自动生成NuGet软件包后缀。
同样，`AssemblyInformationalVersion`也将包含相同的信息（或者至少是正式版本的git hash）。

## 程序集处理器

程序集处理器由Game和Stride目标运行。

它对编译的程序集执行各种转换:
* 生成[DataSerializer](../sources/common/core/Stride.Core/Serialization/DataSerializer.cs)序列化代码（并使用IL-Repack将其合并回组装中）
* 生成[UpdateEngine](../sources/engine/Stride.Engine/Updater/UpdateEngine.cs)代码
* 使用`[ScanAssembly]`扫描类型或属性以快速枚举它们，而无需`Assembly.GetTypes()`
* 优化对[Stride.Core.Utilities](../sources/common/core/Stride.Core/Utilities.cs)的调用
* 自动调用标有[ModuleInitializer](../sources/common/core/Stride.Core/ModuleInitializerAttribute.cs)的方法
* 缓存lambda和其他与[Dispatcher](../sources/common/core/Stride.Core/Threading/Dispatcher.cs)相关的代码生成
* 其他一些内部任务

出于性能原因，它作为MSBuild任务运行（避免重新加载/JIT-ing）。如果您希望使其直接运行可执行文件，请将`StrideAssemblyProcessorDev`设置为`true`。

## 依赖关系

我们需要一种简单的机制来附加一些文件，以将其复制到引用的.dll或.exe旁边，包括内容库和本机库。

结果，添加了`<StrideContent>`和`<StrideNativeLib>`项目类型。

当项目声明它们时，它们将与扩展名为`.ssdeps`的程序集一起保存，以指示引用项目需要复制的内容。

另外，对于特定的`<StrideNativeLib>`，我们会自动将它们复制到适当的文件夹中，并在必要时进行链接。

注意：我们尚未暂时应用它们（项目输出不再包含`.ssdeps`文件，因此直接从可执行文件/应用程序引用非常有用）

## 本机

通过添加对`Stride.Native.targets`的引用，可以轻松构建一些将在所有平台上编译并自动添加到`.ssdeps`文件中的C/C++文件。

### 局限性

似乎使用这些优化方法不适用于阴影复制和探测[私有路径](https://msdn.microsoft.com/en-us/library/823z9h8w(v=vs.110).aspx)。这迫使我们在启动某些工具时将`Direct3D11`特定的程序集复制到顶层Windows文件夹中。这有点不幸，因为它似乎打乱了MSBuild程序集搜索（发生在`$(AssemblySearchPaths)`之前）。结果，在Stride解决方案内部，必须明确地将`<ProjectReference>`添加到特定于图形的程序集中，否则可能会拾取错误的程序集。

这将需要进一步调查，以完全避免这种复制。

## 资产编译器

Games和Stride单元测试都在运行资产编译器，作为创建资产的构建过程的一部分。

# Build

## Overview

This is a technical description what happens in our build and how it is organized. This covers mostly the build architecture of Stride itself.

* [Targets](../Targets) contains the MSBuild target files used by Games
* [sources/common/targets](../sources/common/targets) (generic) and [sources/targets](../sources/targets) (Stride-specific) contains the MSBuild target files used to build Stride itself.

Since 3.1, we switched from our custom build system to the new csproj system with one nuget package per assembly.

We use `TargetFrameworks` to properly compile the different platforms using a single project (Android, iOS, etc...).

Also, we use `RuntimeIdentifiers` to select graphics platform. [MSBuild.Sdk.Extras](https://github.com/onovotny/MSBuildSdkExtras) is used to properly build NuGet packages with multiple `RuntimeIdentifiers` (not supported out of the box).

### Limitations

* Dependencies are per `TargetFramework` and can't be done per `RuntimeIdentifier` (tracked in [NuGet#1660](https://github.com/NuGet/Home/issues/1660)).
* FastUpToDate check doesn't work with multiple `TargetFrameworks` (tracked in [project-system#2487](https://github.com/dotnet/project-system/issues/2487)).

## NuGet resolver

Since we want to package tools (i.e. GameStudio, ConnectionRouter, CompilerApp) with a package that contains only the executable with proper dependencies to other NuGet runtime packages, we use NuGet API to resolve assemblies at runtime.

The code responsible for this is located in [Stride.NuGetResolver](../sources/shared/Stride.NuGetResolver).

Later, we might want to take advantage of .NET Core dependency resolving to do that natively. Also, we might want to use actual project information/dependencies to resolve to different runtime assemblies and better support plugins.

## Versioning

We have 3 places with versions:
- Stride package version (.sdpkg)
- Assembly version (SharedAssemblyInfo.cs) -- should be kept in sync with Stride package version (note: package build will report an error if versions are not matching)
- NuGet package version (.nupkg) -- automatically derived from the Stride package version

During package build, if `StrideOfficialBuild` is not set to true, NuGet package suffix will be automatically generated with `-beta<commits_count_since_last_version_change>-g<git_hash>`
Also, `AssemblyInformationalVersion` will also contain the same information (or at least the git hash for official builds).

## Assembly processor

Assembly processor is run by both Game and Stride targets.

It performs various transforms to the compiled assemblies:
* Generate [DataSerializer](../sources/common/core/Stride.Core/Serialization/DataSerializer.cs) serialization code (and merge it back in assembly using IL-Repack)
* Generate [UpdateEngine](../sources/engine/Stride.Engine/Updater/UpdateEngine.cs) code
* Scan for types or attributes with `[ScanAssembly]` to quickly enumerate them without needing `Assembly.GetTypes()`
* Optimize calls to [Stride.Core.Utilities](../sources/common/core/Stride.Core/Utilities.cs)
* Automatically call methods tagged with [ModuleInitializer](../sources/common/core/Stride.Core/ModuleInitializerAttribute.cs)
* Cache lambdas and various other code generation related to [Dispatcher](../sources/common/core/Stride.Core/Threading/Dispatcher.cs)
* A few other internal tasks

For performance reasons, it is run as a MSBuild Task (avoid reload/JIT-ing). If you wish to make it run the executable directly, set `StrideAssemblyProcessorDev` to `true`.

## Dependencies

We want an easy mechanism to attach some files to copy alongside a referenced .dll or .exe, including content and native libraries.

As a result, `<StrideContent>` and `<StrideNativeLib>` item types were added.

When a project declare them, they will be saved alongside the assembly with extension `.ssdeps`, to instruct referencing projects what needs to be copied.

Also, for the specific case of `<StrideNativeLib>`, we automatically copy them in appropriate folders and link them if necessary.

Note: we don't apply them transitively yet (project output won't contains the `.ssdeps` file anymore so it is mostly useful to reference from executables/apps directly)

## Native

By adding a reference to `Stride.Native.targets`, it is easy to build some C/C++ files that will be compiled on all platforms and automatically added to the `.ssdeps` file.

### Limitations

It seems that using those optimization don't work well with shadow copying and [probing privatePath](https://msdn.microsoft.com/en-us/library/823z9h8w(v=vs.110).aspx). This forces us to copy the `Direct3D11` specific assemblies to the top level `Windows` folder at startup of some tools. This is little bit unfortunate as it seems to disturb the MSBuild assembly searching (happens before `$(AssemblySearchPaths)`). As a result, inside Stride solution it is necessary to explicitely add `<ProjectReference>` to the graphics specific assemblies otherwise wrong ones might be picked up.

This will require further investigation to avoid this copying at all.

## Asset Compiler

Both Games and Stride unit tests are running the asset compiler as part of the build process to create assets.